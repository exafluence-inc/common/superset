from flask import redirect, g, flash, request
from flask_appbuilder.security.views import UserDBModelView,AuthDBView
from superset.security import SupersetSecurityManager
from flask_appbuilder.security.views import expose
from flask_appbuilder.security.manager import BaseSecurityManager
from flask_login import login_user, logout_user
import jwt
import os


class CustomAuthDBView(AuthDBView):
    login_template = 'appbuilder/general/security/login_db.html'

    @expose('/login/', methods=['GET', 'POST'])
    def login(self):
        print("Inside CustomLogin")
        redirect_url = self.appbuilder.get_url_for_index
        if request.args.get('redirect') is not None:
            redirect_url = request.args.get('redirect')
        if request.args.get('token') is not None:
            token=request.args.get('token')
            try:
                jwt.decode(token, key=os.environ.get("JWT_SECRET_KEY","8A6C61D2E97D2E73A082457C0140B1243A1086990CEBE2B948C730B0786F2E72"), algorithms=['HS256', ])
                username=jwt.decode(token, key=os.environ.get("JWT_SECRET_KEY","8A6C61D2E97D2E73A082457C0140B1243A1086990CEBE2B948C730B0786F2E72"), algorithms=['HS256', ])
                print(username)
                username=username['username']
                if username is not None:
                    user = self.appbuilder.sm.find_user(username)
                    login_user(user, remember=False)
                    return redirect(redirect_url)
                elif g.user is not None and g.user.is_authenticated():
                    return redirect(redirect_url)
                else:
                    flash('Unable to auto login', 'warning')
                    return super(CustomAuthDBView, self).login()
            except:
                flash('Unable to auto login', 'warning')
                return super(CustomAuthDBView, self).login()
        else:
            flash('Unable to auto login', 'warning')
            return super(CustomAuthDBView, self).login()


class CustomSecurityManager(SupersetSecurityManager):
    print("Enabled CustomSecurityManager")
    authdbview = CustomAuthDBView
    def __init__(self, appbuilder):
        super(CustomSecurityManager, self).__init__(appbuilder)
